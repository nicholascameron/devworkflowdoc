[Back to home](/README.md)

# Repository Branches Organization 

## Objective

This document aims to explain how branches should be organized when a new project's respository is created.

## GitFlow

Our working methodology is based on **Gitflow Workflow** made popular by [Vincent Driessen at nvie](http://nvie.com/posts/a-successful-git-branching-model/) and extensively used by most developers in coroprate and open source projects.

This methdology is ideal for projects that have a scheduled release cycle and it is based on defining 2 initial branches:

- Master
- Develop

![Master and Develop Branches](/deployment_workflow/images/develop-and-master-branches.svg)

The master branch stores the official release history, and the develop branch serves as an integration branch for features. It's also convenient to tag all commits in the master branch with a version number.

The first step is to complement the default master with a develop branch. A simple way to do this is for one developer to create an empty develop branch locally and push it to the server:

```
git branch develop git push -u origin develop
```

This branch will contain the complete history of the project, whereas master will contain an abridged version. Other developers should now clone the central repository and create a tracking branch for develop.

### How should every developer work with this model?

Each developer is assigned with a new task and ideally for every task a new local branch should be created.

So, after cloning the repo from the develop branch, every developer should do as follows:

```
git checkout -b <feature_branch_name>
```

Local branches should be named after the tasks numbers followed by a hyphen brief description of them, prefixed by the _feature_ or _hotfix_ word.

For example:

```
git checkout -b "feature/s1.1-add-validation-to-customer-record"
```

## Feature Branches

Each new feature should reside in its own branch, which can be pushed to the central repository for backup/collaboration. But, instead of branching off of master, feature branches use develop as their parent branch. When a feature is complete, it gets merged back into develop. Features should never interact directly with master.

![Feature Branches](/deployment_workflow/images/feature-branch-workflow.svg)

### Pushing feature branches

Once the new feature branch has been pushed to the repo, reviewed and approved, it is ready to be _squashed and merged_ to the _develop_ branch.

```
git checkout develop
git merge feature_branch_name
```

**IMPORTANT**

Merging developers branches to the develop branch must be done by the repo owner or the designated person for this merging role.

## Release Branches

![Release Branches](/deployment_workflow/images/release-branches.svg)

Once develop has acquired enough features for a release (or a predetermined release date is approaching), you fork a release branch off of develop. Creating this branch starts the next release cycle, so no new features can be added after this point—only bug fixes, documentation generation, and other release-oriented tasks should go in this branch. Once it's ready to ship, the release branch gets merged into master and tagged with a version number. In addition, it should be merged back into develop, which may have progressed since the release was initiated.

Using a dedicated branch to prepare releases makes it possible for one team to polish the current release while another team continues working on features for the next release. It also creates well-defined phases of development (e.g., it's easy to say, “This week we're preparing for version 4.0,” and to actually see it in the structure of the repository).

Making release branches is another straightforward branching operation. Like feature branches, release branches are based on the develop branch. A new release branch can be created using the following methods.

The command for this would be:

```
git checkout develop
git checkout -b release/0.1.0
```

Once the release is ready to ship, it will get merged it into master and develop, then the release branch will be deleted. It’s important to merge back into develop because critical updates may have been added to the release branch and they need to be accessible to new features. If your organization stresses code review, this would be an ideal place for a pull request.

To finish a release branch, use the following methods:

```
git checkout master
git merge release/0.1.0
```

## Hotfix Branches

![Hotfix Branches](/deployment_workflow/images/hotfix-branch.svg)

Maintenance or “hotfix” branches are used to quickly patch production releases. Hotfix branches are a lot like release branches and feature branches except they're based on master instead of develop. This is the only branch that should fork directly off of master. As soon as the fix is complete, it should be merged into both master and develop (or the current release branch), and master should be tagged with an updated version number.

Having a dedicated line of development for bug fixes lets your team address issues without interrupting the rest of the workflow or waiting for the next release cycle. You can think of maintenance branches as ad hoc release branches that work directly with master. A hotfix branch can be created using the following methods:

```
git checkout master
git checkout -b <hotfix_branch>
```

Similar to finishing a release branch, a hotfix branch gets merged into both master and develop.

```
git checkout master
git merge hotfix_branch
git checkout develop
git merge hotfix_branch
git branch -D hotfix_branch
```

## Further Information

Please feel free to read [this](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
