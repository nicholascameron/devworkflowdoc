# How to start a SDF project

## Description
This tutorial shows how to setup a project using Suitecloud Development Framework. 
Although NetSuite Help has a section that explains how to set it up using Webstorm or Eclipse, this tutorial is based on the use of the commmand line commands and SDK node package in order to make it IDE agnostic.

## @NetSuite
###  Enable SDF
- Log in to NetSuite with an admin role.
- Enable the following features at the Setup > Company > 
   - Enable Features, on the SuiteCloud tab:
   - Item Options (Only when working with Transaction fields)
   - Custom Records
   - Advanced PDF/HTML Templates (Only when working with advanced PDF/HTML templates)
   - Client SuiteScript
   - Server SuiteScript
   - SuiteScript Server Pages (Only when working with SSP Applications)
   - SuiteFlow (Only when working with Workflows)
   - Custom Transactions (Only when working with Custom Transactions)
   - Custom Segments (Only when working with Custom Segments)
   - Web Services (SuiteTalk)
   - Token-Based Authentication
   - SuiteCloud Development Framework

### Assign the Developer Role to the user 

- Log in to NetSuite with an admin role.
- Go to your employee user
- Add the Developer role under the Access subtab

### Install the SuiteCloud Development Integration bundle

- Log in to NetSuite with an admin role.
- Go to Customization > SuiteBundler > Search and Install Bundles
- Search for the **SuiteCloud Development Integration** bundle and install

### Create tokens for your user

- Go to Setup > Users/Roles > Access Token > New Token
   - Application Name: SuiteCloud Development Integration
   - User: <your user>
   - Role: Developer
   - Token Name: <leave by default>
- Copy and save the generated token ID and token secret values (since you will not be able to access them after you close the record)

## @Local Computer

### SuiteCloud CLI for Node.js
You will need to install the [*SuiteCloud Line Interface for Node.js*](https://github.com/oracle/netsuite-suitecloud-sdk) from Github

### Requirements

- [Node.js version 12.14.0 LTS or greater](https://nodejs.org/en/)
- [Oracle JDK version 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)

### Supported versions

| Version in NPM | Version in NetSuite |
| --- | ------ | ---- |
| 1.0.X | 2020.1 |
| 1.1.X | 2020.2 |

### How to check node and java versions

```
javac -version
node -v
```

### Installation

`npm install -g @oracle/suitecloud-cli`

### Usage

`suitecloud <command> <option> <argument>`

*Note:* Check for commands at the repo

### How to start a project

Once you have installed the package locally you are ready to start your SDF project. Go to the command line and execute the following command that will guide your interactively into the creation of your project:

`suitecloud project:create -i`

### Credentials
To setup global credentials you can configure your account as follows

`suitecloud account:setup`

**Notice:** Please refer to the NS Help documentation for the details about the previous command. In any case, you can setup your account by using the Browser but if you plan to setup a *Continuous Integration* pipeline at your repo hosting, you will need to use the tokens that have been previously generated for your user. 

### How to work with SDF

SDF allows you to save every NetSuite object definition and script locally. Therefore, the best way to accomplish this is to use NetSuite UI to create the objects (custom records, scripts, deployments, etc.) and then download them to the project.

### Useful commands

In order to see all commands simply type: `suitecloud`
Every suitcloud option is documented at the NetSuite Help, look for *CLI for Node.js Reference*.

### How to install the [*SuiteCloud Unit Testing*](https://github.com/oracle/netsuite-suitecloud-sdk/tree/master/packages/unit-testing)

- Go to the directory where your SDF project resides
- Run the following command: 

   `npm install --save-dev @oracle/suitecloud-unit-testing jest`

## Considerations about migrations 

It is advisable to work with SDF from the start. However, if objects already exist in the account, they must be downloaded together with their records, in an XML format.
Then, original objects must be removed from the account and uploaded via SDF.

It is important to understand that there cannot be 2 objects (i.e. Custom Record, Custom Field, etc.) with the same ID. If when deploying the code from the console, SDF bumps into a situation where an object that has not been previously deployed by SDF, already exists, it will stop.
