[Back to home](/README.md)

# GIT Workflow

## Objective

The present document is a guide for developers on how to properly use GIT with every project. Also, at the ends it shows a list of most common GIT commands.

## Installing GIT and additional visual tools

_GIT_ must be downloaded from [here](https://git-scm.com/).

[SourceTree](https://www.sourcetreeapp.com/) is an optional open source visual GIT tool

## GIT from the command line

## Daily workflow

### Starting with a project

```shellscript
git clone https://user@bitbucket.org/user/repo.git
cd repo
git pull origin develop
git checkout -b feature/sx.x-task-description
```

When a project is started and tasks have been assigned to a developer, they must:

1. Clone the repository. This link can be retrieved from BITBUCKET. But the user and name of the repository must be provided.
2. Move to the repo directory once it has been locally downloaded
3. Download the _develop_ branch
4. Create the local branch where they will work on. This branch **must** be named: *feature/s<task number>\_<hyphen-separated-task-description>
   Except for hotfixes, where *feature* should be replaced by *hotfix\*.

### Finishing with the task and pushing changes

```
git status
git add .
git commit -m "description about what has been done"
git pull origin develop
git push origin feature/1x.x-task-description
```

The steps should be as follows:

1. Check the status of the changes about to commit. This step is important to avoid other issues like commiting unwanted files.
2. Check if the _develop_ branch has not been updated. If so and there are inconsistencies between the local branch and the remote develop one, the developer should manually go through the changes to combine both before pushing their work.
3. Add changes to GIT
4. Add comments about the work to be commited. At this step is very important to provide detailed information since this text will be pulled by the Pull Request when created, so the more detailed the commit message is, the better. Since it saves time when creating a Pull Request.
5. Finally, the work branch should be pushed.

**IMPORTANT**

The work **must always** be pushed to the developer's branch. _NEVER PUSH CHANGES DIRECTLY TO DEVELOP_

## GIT Settings

In order to confirgure your local GIT you can have a global or per project config file where you specify your user credentials, as well as other configurations.

## Git ignored files

Some files must not be uploaded to the repo, for instance local hidden files from the EDITOR or the computer.
For this purpose, an `.ignore` file must be created at the root of your poroject (or defined globally), to specify files and directories that should be ignored upon pushing your changes.

## Users credentials

User credentials can be set by different ways.
You could create keys in your bitbucket account but the easiest way is to have your credentials in your config file.

The config file is stored at the `.git` hidden directory within your local repo copy.

1. Change to the local _git_ directory
2. Edit the config file
3. Add your Users credentials

```
cd .git
vim config
```

Here is a sample `config` file

```
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
	ignorecase = true
	precomposeunicode = true
[remote "origin"]
	url = https://xxxxx.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
[user]
	name = Your Name
	email = Your email
```

## Best Practices

1. Never push to _develop_ or _master_
2. Always _pull_ the latest changes from develop before pushing yours
3. The _develop branch_ is shared by everyone so it must be 100% operational.
   **Never** push untested code or allow the repo owner to merge feature branches that may break _develop_.
4. Always use clear and descriptive comments in commits. The rule here is this:
   - After you commit your changes a new Pull Request must be generated and when created from a feature branch, it automatically pulls descriptions out of the developer's commits.
   - Automatically generated text should be enough to explain the work done.
5. The Repo owner should remove feature branches after merging them
6. Feature branches should **always** be merged using _Squash and Merge_ to avoid intermediate commits at the _develop branch_

## Useful common GIT commands

### How to unstaged changes

I want to get rid of changes I have done.
**IMPORTANT**  
`git add .` has not been executed

**Solution:**

`git checkout -- .`

This takes you one step behind and undo your changes

---

### How to undo `git add`

I have run `git add .` by mistake

**Solution:**

`git reset`

You could also go to a particular commit

```
git log
git reset --hard <commit number>
```

---

### How to create a branch and checkout at the same time

`git checkout -b <branch name>`

---

### How to rename a branch

If I am at the current branch
`git branch -m <new name>`

From another branch
`git branch -m <oldname> <newname>`

---

### How to download all branches from the remote repo

`git fetch -a`

---

### How to stash changes

This is a very useful command, it allows developers to put what has been done so far on hold, continue with something else and then resume.

The idea is to grab current unstashed changes (meaning, the developer has not executed `git add .`), put them aside for a while (not losing them). Start working on something else or checkout another branch, and retrieving new changes to be merged to the current work.

`git stash`

_starts doing something else_

`git stash apply`

\*retrieves those set aside changes back`

---

### Show branches and sub branches

`git log --all --graph --decorate --oneline -- simplify-by-decoration`
