[Back to home](/README.md)

# How to create a Pull Request

## Initial Concepts

_Pull Request_ is a tool provided by the Repo hosting, (Bitbucket, Github, Gitlab, etc.)

On a regular development workflow, developers are assigned with a set of tasks.

Along the process, developers must create a _feature branch_ for each task they have been assigned to.

Once the task is completed, the developer is ready to push their local feature branch to the repository feature branch.
After this is done, they must always create what it is called a _Pull Request_.

A _Pull request_ allows developers to notify the rest of the team that they have finished with their task.

Therefore, once the task has been reviewed and approved by others, it can finally be merged to the _Development_ branch.

## Creation Process

1. Click on _Pull Request_ from the side menu

       ![Pull Request Side Menu](/deployment_workflow/images/pull-request-menu.png)

2. Click on the **Create pull request** button at the top right.

       Select the name of the local branch you have just pushed at the left selector. Then select where you want to push your branch to, on the right. This is usually: _Develop_

       ![Pull Request Destination](/deployment_workflow/images/pull-request-destination.png)

3. Enter a name for the _Pull Request_. It usually corresponds with the name of the task.

       ![Pull Request Content](/deployment_workflow/images/pull-request-content.png)

       As for the description it should contain a summary of what the task does, as well as a set of screenshots showing what has been done. Screenshots are very useful for this purpose. 

       Additionally, it might be needed to also add comments about needed environment settings on how to properly test the task and/or basic testing that has been done.

4. Add reviewers to your task. Reviewers are composed by pair developers from the same project who must check the quality of the code and solution in general.

5. Finally, the _Close branch_ checkbox could be checked if you would like to remove the _feature branch_ after it has been merged.

6. Once the _PR_ has been created, reviewers _must_ enter their review before the end of the day. This is a task that must be done by everyone along the day.

7. After the _PR_ has been approved, the repo administrator can go ahead and _suash and merge_ the branch. PRs are then saved and remain in the hosting linked to the project so these entries can be used as referential documentation for the project.

_IMPORTANT:_

_Squash and merge_ allows to unify every intermediate commit that might be created during the code correction process with the PR, into one.

## What every developer should know

- Once a PR has been created, every time a new push to the same branch is made, the new code modifications are automatically showed in the PR, which can be checked again by reviewers.

- Reviewers are obliged to make time during the day to check assigned code reviews

- The PR owner must answer PR comments entered by reviewers and make required adjustments in the code if they agree with them. otherwise, enter their justification.

- The PR ownwer could disagree with some comment, answer it and ask for moderation if needed.

- Developers must enter as much useful information as possible to clearly explain what the task is supposed to accomplish.

- Feature branches should be removed from the REPO but they could remain at the local repo of the developer.
