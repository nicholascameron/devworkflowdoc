# How to setup your NS Project Repo for CI

## What is **Continuous Integration**?

Continuous integration (CI) is the practice of automating the integration of code changes from multiple contributors into a single software project. The CI process is comprised of automatic tools that assert the new code’s correctness before integration. A source code version control system is the crux of the CI process. The version control system is also supplemented with other checks like automated code quality tests, syntax style review tools, and more.  

## What is the objective of this methodology

The main objective is to have a working and safe Repo. This methodology guarantees that when a release is pushed to production, no files are left out and manual deployment errors are avoided.

In addition, this also means that the *develop* branch should be always working. Every developer is responsible for creating their integration tests and nothing is pushed either to *master* or *develop* unless integration tests are passed.

Besides, deployment processes are logged in the repo so they can be easily tracked by the developer.

## Bitbucket Pipelines

Since we are using Bitbucket as a repo hosting site, we will be using something called *Pipelines*.

A Pipeline is an *yml* script that is added to the repo and defines the deployments steps. This means that you could define different steps for each branch (this includes provide different set of credentials).

As for credentials, **never include credentials as part of your deployment script**. For this purpose, Bitbucket offers something called [*Secured Environment Variables*](https://support.atlassian.com/bitbucket-cloud/docs/variables-and-secrets/). Your *pipeline* script should use them as follows:

```
# Deployment variables will only work within deployment steps in bitbucket-pipelines.yaml

image: atlassian/default-image:2
pipelines:
  default:
    - step:
          script:
            - <script>
    - step:
          name: Deploy to Test
          deployment: Test
          script:
             - echo $DEPLOYMENT_VARIABLE
```
**Notice:** Bitbucket also offers SSH keys for pipelines but this is not applicable to NetSuite.

### How to create a pipeline for deployment

Here is a `bitbucket-pipelines.yml` template script:

```
image: vickcam/suitecloud-cli-node:1.0

pipelines:
  branches:
    master:
      - step:
          name: Deploying to production
          caches:
            - node
          script:
            - npm install --save-dev @oracle/suitecloud-unit-testing jest
            # Credentials
            - suitecloud account:ci --savetoken --account <account id> --authid <your auth id> --tokenid <TBA token id> --tokensecret <TBA token secret>
            # validation
            - suitecloud project:validate
            # Deployment
            - suitecloud project:deploy
```
### Considerations

- Use secure ENV variables for tokens
- This docker image must be used: `image: vickcam/suitecloud-cli-node:1.0` since it has JDK 11 and Node 12.14 installed.
- Read [Bitbucket documentation](https://support.atlassian.com/bitbucket-cloud/docs/configure-bitbucket-pipelinesyml/) about pipelines scripts in order to be able to customize even further.
- Bitbucket has a [*pipeline script validator*](https://bitbucket-pipelines.prod.public.atl-paas.net/validator)
- Remember this is a [YAML](https://yaml.org) file so indentation is important for every level.

### What to expect

Once you have created your repo at Bitbucket and cloned locally. After your finished and tested your customization, a `bitbucket-pipelines.yml` must be created and placed at the root of your project.

Then, everytime you push your code to the branch where the pipeline is applied to, the script within will be triggered and tests will be run and your code will be automatically deployed to the desired branch.

At Bitbucket, please click on the *Pipelines* option (on the left side bar), in order to check the deployment process.
