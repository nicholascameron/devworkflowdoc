# README

Welcome to the developer documentation repository.

### What is this repository for?

- The objective of this repo is to provide guidance to most of the best practices and methodologies applied to the development and deployment process.
- 1.0
- [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Table of content

**Deployment Workflow**

- [Repository branches organization](/deployment_workflow/repository_branches_organization.md)
- [How to use git (quick recipes)](/deployment_workflow/git-guide.md)
- [How to create _Pull Requests_](/deployment_workflow/how-to-create-a-pull-request.md)
- [SuiteCloud Development Framework](/deployment_workflow/sdf.md)
- [Deployment and continuous integration](/deployment_workflow/ci.md)
- Development environment setup

**Testing Best Practices**

- The importance of Integration Tests
- When and how to create Unit Testing

**Code Review**

- Coding Best Practices
- Eslint
- Code Prettier extension for most popular editors (Webstorm, Vscode, Atom, SublimeText, Eclipse)
- Manual Code Review
